package com.netcracker.data.repository;


import com.netcracker.data.models.CategoryEvents;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryEventsRepository extends JpaRepository<CategoryEvents, Integer> {
}

package com.netcracker.data;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan
@EntityScan(basePackages = "com.netcracker.data.models")
@EnableJpaRepositories(basePackages = "com.netcracker.data.repository")
public class DataConfiguration {
}

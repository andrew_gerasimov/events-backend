package com.netcracker.migration.api;

import com.netcracker.data.models.Events;
import com.netcracker.data.repository.EventsRepository;
import com.netcracker.migration.converter.VkEventConverter;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.database.City;
import com.vk.api.sdk.objects.enums.GroupsType;
import com.vk.api.sdk.objects.groups.Address;
import com.vk.api.sdk.objects.groups.Fields;
import com.vk.api.sdk.objects.groups.Group;
import com.vk.api.sdk.objects.groups.responses.GetByIdLegacyResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Service
public class Main {

    private static EventsRepository eventsRepository = null;

    public Main(@Autowired EventsRepository eventsRepository) {
        Main.eventsRepository = eventsRepository;
    }

    public static void main(String[] args) {
        System.out.println(eventsRepository!=null);
        //VkApiClient obj
        HttpTransportClient httpClient = HttpTransportClient.getInstance();
        VkApiClient vk = new VkApiClient(httpClient);

        //instance of actor
        UserActor actor = new UserActor(633134925,System.getenv("ACCESS_TOKEN"));

        //list of main cities identifiers for all countries
        List<Integer> mainCitiesIds = new ArrayList<>();
        for(int i = 1; i < 235; i++){
            List<City> cities = new ArrayList<>();
            try {
                cities = vk.database().getCities(actor, i)
                        .needAll(false)
                        .execute()
                        .getItems();
            } catch (ApiException | ClientException e) {
                e.printStackTrace();
            }
            for (City city: cities) {
                mainCitiesIds.add(city.getId());
            }
            System.out.println(mainCitiesIds.size());
            try {
                Thread.sleep(340);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //list of events identifiers for main cities
        List<Integer> event_ids = new ArrayList<>();
        for(int i: mainCitiesIds){
            List<Group> events = new ArrayList<>();
            try {
                events = vk.groups().search(actor, String.valueOf('d'))
                        .cityId(i)
                        .type(GroupsType.EVENT)
                        .future(true)
                        .offset(0)
                        .count(1000)
                        .execute()
                        .getItems();
            } catch (ApiException | ClientException e) {
                e.printStackTrace();
            }
            for(Group group: events){
                event_ids.add(group.getId());
            }
            System.out.println("event_ids size: " + event_ids.size());
            try {
                Thread.sleep(334);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("event_ids size final: " + event_ids.size());

        //System.setOut(new java.io.PrintStream(System.out, true, StandardCharsets.UTF_8));

        int counter_id = event_ids.size();
        int offset = 500;

        //list of event groups
        List<GetByIdLegacyResponse> groupInfo = new ArrayList<>();
        while (counter_id > 0) {
            System.out.println("current counter_id is: " + counter_id);
            offset = Math.min(counter_id, 500);
            try {
                groupInfo = vk.groups().getByIdLegacy(actor)
                        .groupIds(event_ids.subList(counter_id-offset,counter_id).toString())
                        .fields(Fields.DESCRIPTION, Fields.CITY, Fields.START_DATE, Fields.FINISH_DATE, Fields.MEMBERS_COUNT, Fields.ACTIVITY, Fields.STATUS, Fields.CONTACTS, Fields.LINKS, Fields.SITE, Fields.COVER)
                        .execute();
            } catch (ApiException | ClientException e) {
                e.printStackTrace();
            }
            for (GetByIdLegacyResponse response : groupInfo) {
                System.out.println(groupInfo);
                try {
                    List<Address> address = vk.groups().getAddresses(actor,response.getId())
                            .count(1)
                            .fields(com.vk.api.sdk.objects.addresses.Fields.ADDRESS, com.vk.api.sdk.objects.addresses.Fields.TITLE)
                            .execute()
                            .getItems();
                    //convert to entity and save
                    Events newEvent = VkEventConverter.convert(response);
                    newEvent.setPlace(address.get(0).getAddress());
                    eventsRepository.save(newEvent);
                } catch (ApiException | ClientException e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(334);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            counter_id = counter_id-500;
        }
    }

}

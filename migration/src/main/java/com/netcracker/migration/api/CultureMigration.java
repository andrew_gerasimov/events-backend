package com.netcracker.migration.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.netcracker.data.models.Events;
import com.netcracker.data.repository.EventsRepository;
import com.netcracker.migration.converter.CultureEventConverter;
import com.netcracker.migration.jackson.CultureEventDeserializer;
import com.netcracker.migration.jackson.NewEventDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Component
public class CultureMigration {

    private static EventsRepository eventsRepository;

    @Autowired
    public CultureMigration(EventsRepository eventsRepository) {
        CultureMigration.eventsRepository = eventsRepository;
    }

    public static void main(String[] args) {

        //ObjectMapper mapper
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();

        module.addDeserializer(NewEventDto.class, new CultureEventDeserializer());
        mapper.registerModule(module);

        List<NewEventDto> event_list = null;
        try {
            event_list = mapper.readValue(
                    new File(Objects.requireNonNull(CultureMigration.class.getClassLoader().getResource("80.json")).getFile()),
                    mapper.getTypeFactory().constructCollectionType(List.class, NewEventDto.class)
            );

            for (NewEventDto eventDto: event_list){
                Events newEvent = CultureEventConverter.convert(eventDto);
                eventsRepository.save(newEvent);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

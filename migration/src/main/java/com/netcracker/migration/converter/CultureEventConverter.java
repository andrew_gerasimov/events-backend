package com.netcracker.migration.converter;

import com.netcracker.data.models.Events;
import com.netcracker.migration.jackson.NewEventDto;
import com.vk.api.sdk.objects.groups.responses.GetByIdLegacyResponse;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

public class CultureEventConverter {

    public static Events convert(NewEventDto response){
        Events event = new Events();
        event.setBriefDescription(response.getName());
        event.setDateTime(response.getTime());
        event.setPlace(response.getPlace());
        event.setFullDescription(response.getDescription());
        return event;
    }
}

package com.netcracker.migration.converter;

import com.netcracker.data.models.Events;
import com.vk.api.sdk.objects.groups.responses.GetByIdLegacyResponse;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

public class VkEventConverter {

    public static Events convert(GetByIdLegacyResponse response){
        Events event = new Events();
        event.setBriefDescription(response.getName());
        //TODO set city by city repository
        //event.setCities(response.getCity());
        event.setFullDescription(response.getDescription());
        event.setDateTime(LocalDateTime.ofInstant(
                Instant.ofEpochMilli(response.getStartDate()),
                TimeZone.getDefault().toZoneId())
        );
        return event;
    }

}

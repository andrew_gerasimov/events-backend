package com.netcracker.migration.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class CultureEventDeserializer extends StdDeserializer<NewEventDto> {

    public CultureEventDeserializer() {
        this(null);
    }

    public CultureEventDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public NewEventDto deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {

        JsonNode eventNode = jp.getCodec().readTree(jp);

        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        JsonNodeType type =  eventNode.get("data").get("general").get("seances").getNodeType();
        //TODO fix java.lang.NullPointerException) (through reference chain: java.util.ArrayList[0])
        LocalDateTime date = LocalDateTime.parse(
                eventNode.get("data").get("general").get("seances").get("start").textValue(),
                inputFormatter
        );

        return new NewEventDto(
                date,
                eventNode.get("data").get("general").get("places").get("address").get("fullAddress").textValue(),
                eventNode.get("data").get("general").get("name").textValue(),
                eventNode.get("data").get("general").get("description").textValue()
        );
    }
}

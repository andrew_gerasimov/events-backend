package com.netcracker.migration.jackson;

import java.time.LocalDateTime;

public class NewEventDto {

    private LocalDateTime time;
    private String place;
    private String name;
    private String description;

    public NewEventDto(LocalDateTime time, String place, String name, String description) {
        this.time = time;
        this.place = place;
        this.name = name;
        this.description = description;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

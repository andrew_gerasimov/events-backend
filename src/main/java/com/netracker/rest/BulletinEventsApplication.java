package com.netracker.rest;


import com.netcracker.data.DataConfiguration;
import com.netcracker.migration.MigrationConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@Import({
		MigrationConfiguration.class,
		DataConfiguration.class

})
//@SpringBootApplication
@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan
public class BulletinEventsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BulletinEventsApplication.class, args);
	}

}
